CREATE DATABASE {{databaseName}};
CREATE USER {{databaseUser}}@'%' IDENTIFIED BY '{{databasePass}}';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, CREATE TEMPORARY TABLES, LOCK TABLES, EXECUTE, CREATE VIEW, SHOW VIEW, CREATE ROUTINE, ALTER ROUTINE, EVENT, TRIGGER ON {{databaseName}}.* TO {{databaseUser}}@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;