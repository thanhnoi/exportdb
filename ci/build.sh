#!/bin/bash

ROOT_PROJ=$(git rev-parse --show-toplevel)

$(cat ${ROOT_PROJ}/.autoenv.zsh | grep VERSION)

GIT_HASH_LOCAL=`git rev-parse --short=8 HEAD`
GIT_HASH=${GIT_HASH:-$GIT_HASH_LOCAL}

CURRENT_VERSION=${VERSION}-${GIT_HASH}

ENV=${ENVIRONMENT}
cd ${ROOT_PROJ}/frontend

if [[ "$ENV" == dev ]]; then
    export API_BASE_URL=https://id-dev.opswat.com
elif [[ "$ENV" == staging ]]; then
    export API_BASE_URL=https://id-staging.opswat.com
elif [[ "$ENV" == prod ]]; then
    export API_BASE_URL=https://id.opswat.com
fi

echo "get secrets from Parameters store"
cd ${ROOT_PROJ}/ssm
npm ci
node index --env "$ENV" get-secrets --file "sso-config.sh" --prefix "export "

cd ${ROOT_PROJ}/frontend
cat environment/sso-config.sh
source environment/sso-config.sh

echo "Building frontend app with ENV: ${ENV} and API_BASE_URL: ${API_BASE_URL}."

export REACT_APP_ENVIRONMENT=${ENV}
export REACT_APP_API_BASE_URL=${API_BASE_URL}
export REACT_APP_CURRENT_VERSION=${CURRENT_VERSION}
npm run build
