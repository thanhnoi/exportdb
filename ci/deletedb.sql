DROP DATABASE {{databaseName}};
DROP USER '{{databaseUser}}'@'%';
FLUSH PRIVILEGES;